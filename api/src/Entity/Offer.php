<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get",
 *         "post"={
 *              "security"="is_granted('ROLE_RECRUITER')",
 *              "denormalization_context"={"groups"={"offer_post_write"}},
 *          }
 *     },
 *     itemOperations={
 *         "get"={"normalization_context"={"groups"={"offer_get_read"}}},
 *         "put"={"security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_RECRUITER') and object.recruiter == user)"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_RECRUITER') and object.recruiter == user)"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_RECRUITER') and object.recruiter == user)"},
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 */
class Offer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offer_post_write","offer_get_read","user_get_read"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"offer_post_write","offer_get_read","user_get_read"})
     */
    private $companyDescription;

    /**
     * @ORM\Column(type="text")
     * @Groups({"offer_post_write","offer_get_read","user_get_read"})
     */
    private $offerDescription;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offer_post_write","offer_get_read","user_get_read"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offer_post_write","offer_get_read","user_get_read"})
     */
    private $contractType;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offer_post_write","offer_get_read","user_get_read"})
     */
    private $workplace;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="offers")
     * @ORM\JoinColumn(nullable=false)
     */
    public $recruiter;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Candidature", mappedBy="offer")
     * @Groups({"offer_get_read"})
     * @MaxDepth(2)
     */
    private $candidatures;

    public function __construct()
    {
        $this->candidatures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCompanyDescription(): ?string
    {
        return $this->companyDescription;
    }

    public function setCompanyDescription(string $companyDescription): self
    {
        $this->companyDescription = $companyDescription;

        return $this;
    }

    public function getOfferDescription(): ?string
    {
        return $this->offerDescription;
    }

    public function setOfferDescription(string $offerDescription): self
    {
        $this->offerDescription = $offerDescription;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getContractType(): ?string
    {
        return $this->contractType;
    }

    public function setContractType(string $contractType): self
    {
        $this->contractType = $contractType;

        return $this;
    }

    public function getWorkplace(): ?string
    {
        return $this->workplace;
    }

    public function setWorkplace(string $workplace): self
    {
        $this->workplace = $workplace;

        return $this;
    }

    public function getRecruiter(): ?User
    {
        return $this->recruiter;
    }

    public function setRecruiter(?User $recruiter): self
    {
        $this->recruiter = $recruiter;

        return $this;
    }

    /**
     * @return Collection|Candidature[]
     */
    public function getCandidatures(): Collection
    {
        return $this->candidatures;
    }

    public function addCandidature(Candidature $candidature): self
    {
        if (!$this->candidatures->contains($candidature)) {
            $this->candidatures[] = $candidature;
            $candidature->setOffer($this);
        }

        return $this;
    }

    public function removeCandidature(Candidature $candidature): self
    {
        if ($this->candidatures->contains($candidature)) {
            $this->candidatures->removeElement($candidature);
            // set the owning side to null (unless already changed)
            if ($candidature->getOffer() === $this) {
                $candidature->setOffer(null);
            }
        }

        return $this;
    }
}
