<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Candidature;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Mailer\Bridge\Google\Transport\GmailSmtpTransport;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mime\Email;

/**
 * Class CandidatureSubscriber
 *
 * @package App\EventSubscriber
 */
final class CandidatureSubscriber implements EventSubscriberInterface
{

    /**
     * {@inheritDoc}
     *
     * @return array[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['sendToCandidate', EventPriorities::POST_WRITE],
        ];
    }

    /**
     * Send Candidature to candidate
     *
     * @param ViewEvent $event
     *
     * @return void
     */
    public function sendToCandidate(ViewEvent $event): void
    {

//        /** @var Candidature $candidature */
//        $candidature = $event->getControllerResult();
//        $method = $event->getRequest()->getMethod();
//
//        if (!$candidature instanceof Candidature || Request::METHOD_POST !== $method) {
//            return;
//        }
//
//        $offer = $candidature->getOffer();
//        $user = $candidature->getCandidate();
//
//        $email = (new Email())
//            ->from($_ENV['GMAIL_EMAIL'])
//            ->to($user->getEmail())
//            ->priority(Email::PRIORITY_HIGH)
//            ->subject('You have a new job proposal')
//            ->html('<p>You have been proposed following job application on CandiKarl:</p>
//            <br><br>
//            <p>Offer: '.$offer->getName().'</p><br>
//            <p>Company: '.$offer->getCompanyDescription().'</p><br>
//            <p>Description: '.$offer->getOfferDescription().'</p><br>
//            <p>Start Date: '.date_format($offer->getStartDate(), 'd/m/y').'</p><br>
//            <p>Contract Type: '.$offer->getContractType().'</p><br>
//            <p>Workplace: '.$offer->getWorkplace().'</p><br>
//            <p>To apply, please add following token: '.$candidature->getToken().'</p>
//            ');
//
//        $transport = new GmailSmtpTransport($_ENV['GMAIL_EMAIL'], $_ENV['GMAIL_PASSWORD']);
//        $mailer = new Mailer($transport);
//        $mailer->send($email);

    }
}
