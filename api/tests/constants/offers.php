<?php

$valid = '{
    "name": "Vetixy",
    "companyDescription": "Doctolib pour les vétérinaires",
    "offerDescription": "Développeur Fullstack PHP REACT NATIVE",
    "startDate": "2020-06-03T22:13:31.238Z",
    "contractType": "CDI",
    "workplace": "Ecole 42"
}';

$missingProperty = '{
    "companyDescription": "Doctolib pour les vétérinaires",
    "offerDescription": "Développeur Fullstack PHP REACT NATIVE",
    "startDate": "2020-06-03T22:13:31.238Z",
    "contractType": "CDI",
    "workplace": "Ecole 42"
}';

$tooLongName = '{
    "name": "VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy
    VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy
    VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy",
    "companyDescription": "Doctolib pour les vétérinaires",
    "offerDescription": "Développeur Fullstack PHP REACT NATIVE",
    "startDate": "2020-06-03T22:13:31.238Z",
    "contractType": "CDI",
    "workplace": "Ecole 42"
}';

$update = '{
    "offerDescription": "Développeur Fullstack PHP REACT NATIVE et API PLATFORM"
}';

$updateWithLongName = '{
    "name": "VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy
    VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy
    VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy"
}';

$completeUpdate = '{
    "recruiter": "/users/{{user_recruiter_fixed_1.id}}",
    "name": "Wizards",
    "companyDescription": "Poudlard",
    "offerDescription": "Savoir faire de la magie noire",
    "startDate": "2020-06-03T22:13:31.238Z",
    "contractType": "CDI",
    "workplace": "Ecole 42"
}';

$tooLongNameUpdate = '{
    "recruiter": "/users/{{user_recruiter_fixed_1.id}}",
    "name": "VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy
    VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy
    VetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixyVetixy",
    "companyDescription": "Doctolib pour les vétérinaires",
    "offerDescription": "Développeur Fullstack PHP REACT NATIVE",
    "startDate": "2020-06-03T22:13:31.238Z",
    "contractType": "CDI",
    "workplace": "Ecole 42"
}';