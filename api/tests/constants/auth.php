<?php

// Valid user
$validUser = '{
        "email": "new@new.com",
        "password": "new",
        "lastname": "new",
        "firstname": "user",
        "gender": "male",
        "age": 21,
        "address": "42, av jean moulin"
    }';

// Email is not valid
$notValidEmailUser = '{
        "email": "new",
        "password": "new",
        "lastname": "new",
        "firstname": "user",
        "gender": "male",
        "age": 21,
        "address": "42, av jean moulin"
    }';

// Password is too short
$notValidPasswordSizeUser = '{
        "email": "new@new.com",
        "password": "a",
        "lastname": "new",
        "firstname": "user",
        "gender": "male",
        "age": 21,
        "address": "42, av jean moulin"
    }';

// Password is empty
$notValidPasswordUser = '{
        "email": "new@new.com",
        "password": "",
        "lastname": "new",
        "firstname": "user",
        "gender": "male",
        "age": 21,
        "address": "42, av jean moulin"
    }';

// Payload
$notFullPayloadUser = '{
        "email": "new@new.com",
        "lastname": "new",
        "firstname": "user",
    }';

// Conflictual email
$conflictualEmailUser = '{
        "email": "admin@admin.com",
        "password": "bad",
        "lastname": "new",
        "firstname": "user",
        "gender": "male",
        "age": 21,
        "address": "42, av jean moulin"
    }';