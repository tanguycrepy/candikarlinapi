<?php

namespace App\Tests\Behat\Manager;

class AuthManager
{
    private $token;

    /**
     * The user to use with HTTP basic authentication
     *
     * @var string
     */
    public $authUser;

    /**
     * The password to use with HTTP basic authentication
     *
     * @var string
     */
    public $authPassword;

    public function setToken($token){
        $this->token = $token;
    }

    public function getToken(){
        return $this->token;
    }

    public function createConnectionPayload(string $login, string $mdp){
        $this->authUser = $login;
        $this->authPassword = $mdp;
        return json_decode('{"email":"' . $login . '", "password":"' . $mdp . '"}');
    }

    public function handleConnection($responseContent){
        $this->setToken(json_decode($responseContent, true)["token"]);
    }

}