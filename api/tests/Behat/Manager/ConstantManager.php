<?php

namespace App\Tests\Behat\Manager;

class ConstantManager
{
    private $referenceManager;

    public function __construct(ReferenceManager $referenceManager)
    {
        $this->referenceManager = $referenceManager;
    }

    public function load(string $file, string $constant)
    {
        include($file);
        $data = ${$constant};
        $this->referenceManager->replaceRegex($data);
        return $data;
    }
}
