<?php

namespace App\Tests\Behat\Manager;

class ReferenceManager
{
    private $reference = [];

    public function addRef($ref, $object)
    {
        $this->reference[$ref] = $object;
    }

    public function getRefContent($ref)
    {
        return $this->reference[$ref];
    }

    public function replaceRegex(&$resource) {
        $re = '/{{(.*?)}}/m';
        preg_match_all($re, $resource, $matches, PREG_SET_ORDER, 0);
        if ($matches) {
            foreach ($matches as $match) {
                if (strpos($match[1], ".")) {
                    $val = explode(".", $match[1]);
                    $function = "get".ucfirst($val[1]);
                    $resource = str_replace($match[0], $this->getRefContent($val[0])->$function(), $resource);
                } else {
                    $resource = str_replace($match[0], $this->getRefContent($match[1])->getId(), $resource);
                }
            }
        }
    }
}
