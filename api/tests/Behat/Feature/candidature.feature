Feature: _Candidature_
  Background:
    Given the following fixtures files are loaded:
      | user        |
      | offers      |
      | candidature |

  Scenario: Test resource unavailable if not connected
    When I request "GET /candidatures"
    Then the response status code should be 401

  Scenario: Test resource unavailable if user connected is not admin
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When I request "GET /candidatures"
    Then the response status code should be 403

  Scenario: Test resource available if user connected is admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    When I request "GET /candidatures"
    Then the response status code should be 200

  Scenario: Test resource unavailable if user connected is not the owner
    Given I authenticate with user "recruiter2@recruiter.com" and password "automdp"
    When I request "GET /candidatures/{{candidature_fixed_1}}"
    Then the response status code should be 403

  Scenario: Test resource available if user connected is the owner
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When I request "GET /candidatures/{{candidature_fixed_1.id}}"
    Then the response status code should be 200

  Scenario: Test get candidature with token
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    When I request "GET /addCandidate/{{candidature_fixed_1.token}}"
    Then the response status code should be 200

  Scenario: Test add candidature
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    Given the valid payLoad is loaded from candidatures constant file
    When I request "POST /candidatures"
    Then the response status code should be 201
