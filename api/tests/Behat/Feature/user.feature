Feature: _User_
  Background:
    Given the following fixtures files are loaded:
      | user        |

  Scenario: Test resources users unavailable if not connected
    When I request "GET /users"
    Then the response status code should be 401

  Scenario: Test resource users unavailable if not connected
    When I request "GET /users/{{user_recruiter_fixed_1}}"
    Then the response status code should be 401

  Scenario: Test resources users available if connected as admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    When I request "GET /users"
    Then the response status code should be 200

  Scenario: Test resource users available if connected as admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    When I request "GET /users/{{user_recruiter_fixed_1}}"
    Then the response status code should be 200
    And the "@id" property should equal "/users/{{user_recruiter_fixed_1.id}}"

  Scenario: Test resources users not available if connected as recruiter
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When I request "GET /users"
    Then the response status code should be 401

  Scenario: Test resource users available if connected as recruiter
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When I request "GET /users/{{user_recruiter_fixed_1}}"
    Then the response status code should be 200
    And the "@id" property should equal "/users/{{user_recruiter_fixed_1.id}}"

  Scenario: Test resources users not available if connected as candidate
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    When I request "GET /users"
    Then the response status code should be 401

  Scenario: Test resource users not available if connected as candidate
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    When I request "GET /users/{{user_recruiter_fixed_1}}"
    Then the response status code should be 403

  # PUT
  Scenario: Test existing users cannot be updated if not connected
    Given the validUser payLoad is loaded from user constant file
    When I request "PUT /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 401

  Scenario: Test existing user can be updated by admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    And the validUser payLoad is loaded from user constant file
    When I request "PUT /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 200
    And the "lastname" property should equal "KRAKRA"

  Scenario: Test existing user cannot be updated by someone else
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    And the validUser payLoad is loaded from user constant file
    When I request "PUT /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 403

  Scenario: Test existing user can be updated by himself
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    And the validUser payLoad is loaded from user constant file
    When I request "PUT /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 200
    And the "lastname" property should equal "KRAKRA"

  # DELETE
  Scenario: Test existing offer can't be deleted if not connected
    When I request "DELETE /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 401

  Scenario: Test existing user can't be deleted if connected as candidate
    Given I authenticate with user "candidate2@candidate.com" and password "automdp"
    When I request "DELETE /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 403

  Scenario: Test existing user can be deleted if connected as admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    When I request "DELETE /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 204

  Scenario: Test existing user can't be deleted if connected as wrong user
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When I request "DELETE /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 403

  Scenario: Test existing user can be deleted if connected as correct user
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    When I request "DELETE /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 204

  # PATCH
  Scenario: Test existing users cannot be updated if not connected
    Given the validUser payLoad is loaded from user constant file
    When I request "PATCH /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 401

  Scenario: Test existing user can be updated by admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    And the validUser payLoad is loaded from user constant file
    When I request "PATCH /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 200
    And the "lastname" property should equal "KRAKRA"

  Scenario: Test existing user cannot be updated by someone else
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    And the validUser payLoad is loaded from user constant file
    When I request "PATCH /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 403

  Scenario: Test existing user can be updated by himself
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    And the validUser payLoad is loaded from user constant file
    When I request "PATCH /users/{{user_candidate_fixed_1.id}}"
    Then the response status code should be 200
    And the "lastname" property should equal "KRAKRA"