Feature: _Auth_
  Background:
    Given the following fixtures files are loaded:
      | user           |

  Scenario: Test register
    Given the validUser payLoad is loaded from auth constant file
    Given I request "POST /users"
    Then the response status code should be 201

  Scenario: Test connection as recruiter
    When I authenticate with user "admin@admin.com" and password "automdp"
    When the response status code should be 200

  Scenario: Test connection as admin
    When I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When the response status code should be 200

  Scenario: Test connection as candidate
    When I authenticate with user "candidate@candidate.com" and password "automdp"
    When the response status code should be 200

  Scenario: Test email incorrect
    Given the notValidEmailUser payLoad is loaded from auth constant file
    When I request "POST /users"
    Then the response status code should be 400

  Scenario: Test password isn't strong enough
    Given the notValidPasswordSizeUser payLoad is loaded from auth constant file
    When I request "POST /users"
    Then the response status code should be 400

  Scenario: Test password is null
    Given the notValidPasswordUser payLoad is loaded from auth constant file
    When I request "POST /users"
    Then the response status code should be 400

  Scenario: Test payload isn't full
    Given the notFullPayloadUser payLoad is loaded from auth constant file
    When I request "POST /users"
    Then the response status code should be 400

  Scenario: Test email is in conflict
    Given the conflictualEmailUser payLoad is loaded from auth constant file
    When I request "POST /users"
    Then the response status code should be 409
