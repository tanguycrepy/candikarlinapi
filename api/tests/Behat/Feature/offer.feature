Feature: _Offer_
  Background:
    Given the following fixtures files are loaded:
      | user           |
      | offers         |
#      | candidature    |

  # GET

  Scenario: Test can't fetch offer if not connected
    When I request "GET /offers/{{offer_owner_1.id}}"
    Then the response status code should be 401
    And the "name" property should not exist

  Scenario: Test can fetch offer if connected as candidate
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    When I request "GET /offers/{{offer_owner_1.id}}"
    Then the response status code should be 200
    And the "name" property should exist

  Scenario: Test can fetch offer if connected as recruiter
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When I request "GET /offers/{{offer_owner_1.id}}"
    Then the response status code should be 200
    And the "name" property should exist
  
  Scenario: Test can fetch offer if connected as recruiter
    Given I authenticate with user "admin@admin.com" and password "automdp"
    When I request "GET /offers/{{offer_owner_1.id}}"
    Then the response status code should be 200
    And the "name" property should exist

  # GET ALL

  Scenario: Test can't fetch offers if not connected
    When I request "GET /offers"
    Then the response status code should be 401
    And the "hydra:member" property should not exist
    And the "hydra:totalItems" property should not exist

  Scenario: Test can fetch offers if connected as admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    When I request "GET /offers"
    Then the response status code should be 200
    And the "hydra:member" property should be an array
    And the "hydra:member" property should contain 30 items
    And the "hydra:totalItems" property should be an integer equalling "32"

  Scenario: Test can't fetch offers if connected as recruiter
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When I request "GET /offers"
    Then the response status code should be 401

  Scenario: Test can't fetch offers if connected as candidate
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    When I request "GET /offers"
    Then the response status code should be 401

  # POST

  Scenario: Test new offer can't be saved if not connected
    When I request "POST /offers"
    Then the response status code should be 401

  Scenario: Test new offer can't be saved if connected as admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    And the valid payLoad is loaded from offers constant file
    When I request "POST /offers"
    Then the response status code should be 403

  Scenario: Test new offer can't be saved if connected as candidate
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    And the valid payLoad is loaded from offers constant file
    When I request "POST /offers"
    Then the response status code should be 403

  Scenario: Test new offer is saved if connected as recruiter
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    And the valid payLoad is loaded from offers constant file
    When I request "POST /offers"
    Then the response status code should be 201
  
  Scenario: Test new offer is not saved if there is a missing property
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    And the missingProperty payLoad is loaded from offers constant file
    When I request "POST /offers"
    Then the response status code should be 500
  
  Scenario: Test new offer is not saved if there is a too long name
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    And the tooLongName payLoad is loaded from offers constant file
    When I request "POST /offers"
    Then the response status code should be 400

  # PUT

  Scenario: Test existing offer can't be put updated if not connected
    Given the update payLoad is loaded from offers constant file
    When I request "PUT /offers/{{offer_owner_1.id}}"
    Then the response status code should be 401
    And the "offerDescription" property should not exist

  Scenario: Test existing offer can't be put updated if connected as candidate
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    And the update payLoad is loaded from offers constant file
    When I request "PUT /offers/{{offer_owner_1.id}}"
    Then the response status code should be 403
    And the "offerDescription" property should not exist
  
  Scenario: Test existing offer can be put updated if connected as admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    And the update payLoad is loaded from offers constant file
    When I request "PUT /offers/{{offer_owner_1.id}}"
    Then the response status code should be 200
    And the "offerDescription" property should equal "Développeur Fullstack PHP REACT NATIVE et API PLATFORM"

  Scenario: Test existing offer can't be put updated if connected as wrong recruiter
    Given I authenticate with user "recruiter2@recruiter.com" and password "automdp"
    And the update payLoad is loaded from offers constant file
    When I request "PUT /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 403
    And the "offerDescription" property should not exist

  Scenario: Test existing offer can be put updated if connected as correct recruiter
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    And the update payLoad is loaded from offers constant file
    When I request "PUT /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 200
    And the "offerDescription" property should equal "Développeur Fullstack PHP REACT NATIVE et API PLATFORM"

  Scenario: Test existing offer can't be put updated if there is a too long name
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    And the updateWithLongName payLoad is loaded from offers constant file
    When I request "PUT /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 400

  # DELETE
  
  Scenario: Test existing offer can't be deleted if not connected
    When I request "DELETE /offers/{{offer_owner_1.id}}"
    Then the response status code should be 401

  Scenario: Test existing offer can't be deleted if connected as candidate
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    When I request "DELETE /offers/{{offer_owner_1.id}}"
    Then the response status code should be 403
  
  Scenario: Test existing offer can be deleted if connected as admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    When I request "DELETE /offers/{{offer_owner_1.id}}"
    Then the response status code should be 204

  Scenario: Test existing offer can't be deleted if connected as wrong recruiter
    Given I authenticate with user "recruiter2@recruiter.com" and password "automdp"
    When I request "DELETE /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 403

  Scenario: Test existing offer can be deleted if connected as correct recruiter
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    When I request "DELETE /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 204
  
  # PATCH

  Scenario: Test existing offer can't be patch updated if not connected
    Given the completeUpdate payLoad is loaded from offers constant file
    When I request "PUT /offers/{{offer_owner_1.id}}"
    Then the response status code should be 401
    And the "name" property should not exist
    And the "companyDescription" property should not exist
    And the "offerDescription" property should not exist
    And the "startDate" property should not exist
    And the "contractType" property should not exist
    And the "workplace" property should not exist

  Scenario: Test existing offer can't be patch updated if connected as candidate
    Given I authenticate with user "candidate@candidate.com" and password "automdp"
    And the completeUpdate payLoad is loaded from offers constant file
    When I request "PATCH /offers/{{offer_owner_1.id}}"
    Then the response status code should be 403
    And the "name" property should not exist
    And the "companyDescription" property should not exist
    And the "offerDescription" property should not exist
    And the "startDate" property should not exist
    And the "contractType" property should not exist
    And the "workplace" property should not exist
  
  Scenario: Test existing offer can be patch updated if connected as admin
    Given I authenticate with user "admin@admin.com" and password "automdp"
    And the completeUpdate payLoad is loaded from offers constant file
    When I request "PATCH /offers/{{offer_owner_1.id}}"
    Then the response status code should be 200
    And the "name" property should equal "Wizards"
    And the "companyDescription" property should equal "Poudlard"
    And the "offerDescription" property should equal "Savoir faire de la magie noire"
    And the "startDate" property should equal "2020-06-03T00:00:00+00:00"
    And the "contractType" property should equal "CDI"
    And the "workplace" property should equal "Ecole 42"

  Scenario: Test existing offer can't be patch updated if connected as wrong recruiter
    Given I authenticate with user "recruiter2@recruiter.com" and password "automdp"
    And the completeUpdate payLoad is loaded from offers constant file
    When I request "PATCH /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 403
    And the "name" property should not exist
    And the "companyDescription" property should not exist
    And the "offerDescription" property should not exist
    And the "startDate" property should not exist
    And the "contractType" property should not exist
    And the "workplace" property should not exist

  Scenario: Test existing offer can be patch updated if connected as correct recruiter
    Given I authenticate with user "recruiter@recruiter.com" and password "automdp"
    And the completeUpdate payLoad is loaded from offers constant file
    When I request "PATCH /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 200
    And the "name" property should equal "Wizards"
    And the "companyDescription" property should equal "Poudlard"
    And the "offerDescription" property should equal "Savoir faire de la magie noire"
    And the "startDate" property should equal "2020-06-03T00:00:00+00:00"
    And the "contractType" property should equal "CDI"
    And the "workplace" property should equal "Ecole 42"

  Scenario: Test existing offer can not be patch updated if there is a too long name
    Given I authenticate with user "recruiter2@recruiter.com" and password "automdp"
    And the tooLongNameUpdate payLoad is loaded from offers constant file
    When I request "PATCH /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 403
    And the "name" property should not exist
    And the "companyDescription" property should not exist
    And the "offerDescription" property should not exist
    And the "startDate" property should not exist
    And the "contractType" property should not exist
    And the "workplace" property should not exist
  
  Scenario: Test existing offer can not be patch updated if there is missing properties
    Given I authenticate with user "recruiter2@recruiter.com" and password "automdp"
    And the update payLoad is loaded from offers constant file
    When I request "PATCH /offers/{{offer_owner_restricted_1.id}}"
    Then the response status code should be 403
    And the "name" property should not exist
    And the "companyDescription" property should not exist
    And the "offerDescription" property should not exist
    And the "startDate" property should not exist
    And the "contractType" property should not exist
    And the "workplace" property should not exist