<?php

namespace App\Tests\Behat\Context\Traits;

use App\Tests\Behat\Manager\AuthManager;
use GuzzleHttp\Psr7\Request;

trait AuthTrait
{

    /**
     * @var AuthManager
     */
    protected $authManager;

    /**
     * @Given /^I authenticate with user "([^"]*)" and password "([^"]*)"$/
     */
    public function iAuthenticateWithEmailAndPassword($email, $password)
    {
        $this->requestPayload = $this->authManager->createConnectionPayload($email, $password);
        $this->iRequest("post", "/login");
        $this->authManager->handleConnection($this->getLastResponse()->getContent());
    }
}
