<?php

namespace App\Tests\Behat\Context\Traits;

use App\Tests\Behat\Manager\ConstantManager;

trait ConstantsTrait
{
    /**
     * @var ConstantManager
     */
    private ConstantManager $constantManager;

    /**
     * @Given the :constant payLoad is loaded from :files constant file
     */
    public function thePayloadIsLoadedFromConstantFile(string $constant, string $file)
    {
        $this->requestPayload = json_decode($this->constantManager->load('./tests/constants/'.$file.'.php',$constant), true);
    }
}
