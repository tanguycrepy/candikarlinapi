# CandiKarlin API
 
Candidate and manage job purposes with Api-Platform

# Install

- `$ yarn install` in /admin folder
- `$ docker-compose pull`
- `$ docker-compose build --no-cache`
- `$ docker-compose up -d`

# Routes 
- home : https://localhost/
- api : https://localhost:8443/docs
- admin : https://localhost:444/

# Api setup 
    docker-compose exec php composer install

	docker-compose exec php bin/console d:d:d --force
	docker-compose exec php bin/console d:d:c
	docker-compose exec php bin/console d:s:u --force
	
	docker-compose exec php bin/console d:f:l -n

## Login to api-platform
- Please make sure you run the datafixture to get admin user
- Go to [https://localhost:8443/docs](https://localhost:8443/docs)
- Click "Try it out" button on Token `POST /login` root
- Request body should be
```json
{
  "email": "admin@admin.com",
  "password": "admin"
}
```
- then copy the token value into response body like below 
```
{
  "token": "TOKEN_VALUE"
}
```
- Then click on "Authorize" (top of the page)
- Fill "value" field with `Bearer TOKEN_VALUE`
